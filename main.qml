import QtQuick
import QtQuick.Window
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.12

Window {
    id: window
    width: 640
    height: 480
    visible: true
    title: qsTr("Калькулятор")
    color: "gray"
    minimumWidth: 300
    minimumHeight: 300


    Connections {
        target: calc
        onChangeValue1: {
            text1.text = calc.value1
        }
        onChangeValue2: {
            text2.text = calc.value2
        }
        onChangeOperation: {
            operation.text = calc.operation
        }
    }

    ColumnLayout{
        anchors.fill: parent

        spacing: 5
        RowLayout{
            Layout.fillWidth: true

            spacing: 5
            Rectangle{


                Layout.leftMargin: 5
                Layout.fillWidth: true
                height: 30
                border.color: "black"
                Layout.topMargin: 5
                Text{
                    id: text1
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                    anchors.rightMargin: 5
                    anchors.leftMargin: 5
                    text: "0"
                    font.pointSize: 12
                }
            }
            Rectangle{
                Layout.topMargin: 5
                width: 30
                height: 30
                border.color: "black"
                Text{
                    id: operation
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    anchors.rightMargin: 5
                    anchors.leftMargin: 5
                    font.pointSize: 18
                }
            }
            Rectangle{
                height: 30
                border.color: "black"
                Layout.rightMargin: 5
                Layout.topMargin: 5
                Layout.fillWidth: true

                Text{
                    id: text2
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                    anchors.rightMargin: 5
                    anchors.leftMargin: 5
                    text: "0"
                    font.pointSize: 12
                }
            }
        }

        Rectangle{

            Layout.leftMargin: 5
            Layout.rightMargin: 5
            Layout.fillWidth: true

            height: 30
            border.color: "black"
            Text{
                id:result
                anchors.fill: parent
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
                anchors.rightMargin: 5
                anchors.leftMargin: 5
                text: "0"
                font.pointSize: 12
            }
        }
        GridLayout{
            Layout.bottomMargin: 5
            Layout.rightMargin: 5
            Layout.leftMargin: 5
            Layout.fillHeight: true
            Layout.fillWidth: true
            Button {
                Layout.column: 0
                Layout.row:0
                text: "C"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10

                Layout.fillHeight: true
                Layout.fillWidth: true
                background: Rectangle {
                    color: "#ff4300"
                    radius: 5
                    border.width: 1
                }
                Connections {
                    onClicked: {
                        result.text = "0";
                        calc.reset()
                    }
                }
            }
            Button {
                Layout.column: 0
                Layout.row:1
                text: "1"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10

                Layout.fillHeight: true
                Layout.fillWidth: true

                Connections {
                    onClicked: calc.addNum("1")
                }

            }
            Button {

                Layout.column: 1
                Layout.row:1
                text: "2"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.addNum("2")
                }
            }
            Button {
                Layout.column: 2
                Layout.row:1
                text: "3"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.addNum("3")
                }
            }
            Button {
                Layout.column: 0
                Layout.row:2
                text: "4"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.addNum("4")
                }
            }
            Button {
                Layout.column: 1
                Layout.row:2
                text: "5"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.addNum("5")
                }
            }
            Button {
                Layout.column: 2
                Layout.row:2
                text: "6"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.addNum("6")
                }
            }
            Button {
                Layout.column: 0
                Layout.row:3
                text: "7"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.addNum("7")
                }
            }
            Button {
                Layout.column: 1
                Layout.row:3
                text: "8"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.addNum("8")
                }
            }
            Button {

                Layout.column: 2
                Layout.row:3
                text: "9"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.addNum("9")
                }
            }
            Button {
                Layout.column: 0
                Layout.row:4
                text: "0"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.addNum("0")
                }
            }
            Button {
                Layout.column: 1
                Layout.row:4
                text: "\xB1"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.addNum("-")
                }
            }
            Button {
                Layout.column: 2
                Layout.row:4
                text: "."
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.addNum(".")
                }
            }
            Button {
                Layout.column: 3
                Layout.row:4
                text: "="
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: result.text = calc.sum
                }
            }
            Button {
                Layout.column: 3
                Layout.row:0
                text: "+"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.setOperation("+")
                }
            }
            Button {
                Layout.column: 3
                Layout.row:1
                text: "-"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.setOperation("-")
                }
            }
            Button {
                Layout.column: 3
                Layout.row:2
                text: "*"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.setOperation("*")
                }
            }
            Button {
                Layout.column: 3
                Layout.row:3
                text: "/"
                font.pointSize: parent.width < parent.height ? parent.width / 10 : parent.height /10
                Layout.fillHeight: true
                Layout.fillWidth: true
                Connections {
                    onClicked: calc.setOperation("/")
                }
            }
        }
    }
}


