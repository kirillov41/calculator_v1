#pragma once
#include <QObject>
#include <string>

class Calc : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString value1 READ getValue1)
    Q_PROPERTY(QString value2 READ getValue2)
    Q_PROPERTY(QString operation READ getOperation)
    Q_PROPERTY(QString sum READ getSum)
private:

    QString value2 = "0";
    QString value1 = "0";
    double sum = 0;
    QString operation = "";
    bool isEquals = false;

    void setValue(QString& value, QString val){
        if (val== "." && value.toStdString().find(".")!=std::string::npos) return;
        if (value=="0" && val != "-")
            if (val == ".") value += val;
            else value = val;
        else if (val == "-" && value!="0"){
            std::string tmp = value.toStdString();
            if (tmp.find("-")==std::string::npos){
                tmp.insert(0,"-");
                value = QString::fromStdString(tmp);
            }else {
                tmp.erase(0,1);
                value = QString::fromStdString(tmp);
            }
        }
        else if (value.size() < 10 && val != "-") value+= val;
    }

public:

    QString getValue1() {
        return value1;
    }

    QString getValue2() {
        return value2;
    }
    QString getOperation() {
        return operation;
    }
    QString getSum(){
        if (operation == "") return "0";
        isEquals = true;
        if (operation == "+")
            sum = std::stod(value1.toStdString()) + std::stod(value2.toStdString());
        else if (operation == "-")
            sum = std::stod(value1.toStdString()) - std::stod(value2.toStdString());
        else if (operation == "*")
            sum = std::stod(value1.toStdString()) * std::stod(value2.toStdString());
        else if (operation == "/"){
            if (value2 == "0"){
                return "ERROR";
            }else sum = std::stod(value1.toStdString()) / std::stod(value2.toStdString());
        }
        int sumInt = sum;
        if (sum - sumInt != 0) return QString::fromStdString(std::to_string(sum));
        else return QString::fromStdString(std::to_string(sumInt));
    }

public slots:
    void addNum(QString val) {
        if (isEquals) return;
        if (operation == ""){
            setValue(value1, val);
            emit changeValue1();
        }
        else {
            setValue(value2, val);
            emit changeValue2();
        }
    }

    void setOperation(QString val){
        if (operation==""){
            operation = val;
            emit changeOperation();
        }
    }

    void reset(){
        value1 = "0";
        value2 = "0";
        sum = 0;
        operation = "";
        isEquals = false;
        emit changeValue1();
        emit changeValue2();
        emit changeOperation();
    }

signals:
    void changeValue1();
    void changeValue2();
    void changeOperation();
};

